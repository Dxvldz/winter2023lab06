public class Jackpot {
    public static void main(String[]args) {
        System.out.println("Welcome user to the Jackpot Game : Simpler Version");
        Board board = new Board();
        Boolean gameOver = false;
        int numOfTilesClosed = 0;

        while (!gameOver) {
            System.out.println(board);
            
           if (board.playATurn()) {
            gameOver = true;
           }
           else {
            numOfTilesClosed ++;
           }
        }
        
        if (numOfTilesClosed >= 7) {
            System.out.println("You've reached the Jackpot!! You won!");
        }
        else {
            System.out.println("You lost...");
        }
    }
}