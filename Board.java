public class Board {
    private Die[] dices = new Die[2];
    private boolean[] tiles;

    public Board() {
        this.dices[0] = new Die();
        this.dices[1] = new Die();
        this.tiles = new boolean[12];
    }
    
    public String toString() {
        String value="";
    
        for (int i = 0 ; i < tiles.length ; i++ ) {
            if (!tiles[i]) {
                value = value + " " + (i+1) + " ";
            }
            else {
                value = value + " X ";
            }
        }
        return value;
    }

    public boolean playATurn() {
        int sumOfDice;
        dices[0].roll();
        dices[1].roll();

        System.out.println(dices[0]);
        System.out.println(dices[1]);

        sumOfDice = dices[0].getFaceValue() + dices[1].getFaceValue();
        if (!this.tiles[sumOfDice - 1]) {
            this.tiles[sumOfDice - 1] = true;
            System.out.println("Closing tile equal to the sum : " + sumOfDice);
            return false;
        }

        else if (!this.tiles[dices[0].getFaceValue()-1]) {
            this.tiles[dices[0].getFaceValue()-1] = true;
            System.out.println("Closing tile with the same value as die one : " + dices[0].getFaceValue());
            return false;
        }

        else if (!this.tiles[dices[1].getFaceValue()-1]) {
            this.tiles[dices[1].getFaceValue()-1] = true;
            System.out.println("Closing tile with the same value as die two : " + dices[1].getFaceValue());
            return false;
        }

        else {
            System.out.println("All the tiles for these valuees are already shut");
            return true;
        }

    }


}
